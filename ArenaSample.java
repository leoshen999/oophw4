import ntu.csie.oop13spring.POOFight;
import ntu.csie.oop13spring.POOArena;
import ntu.csie.oop13spring.POOCoordinate;
import ntu.csie.oop13spring.POOPet;
import ntu.csie.oop13spring.POOAction;
import ntu.csie.oop13spring.POOSkill;
import java.util.*;
public class ArenaSample extends POOArena
{
	
	public ArrayList<POOCoordinate> allcoordinate = new ArrayList<POOCoordinate>(0);
	
	public int X=16,Y=16,turn=0,round=1;
	
	public POOPet[] parr;
	
	public char[][] map;
	
	public ArenaSample()
	{
		
		System.out.print("\033[2J");
		System.out.print("\033[0;0H");
		
	}
	public boolean fight()
	{
		if(round==1)
		{
			parr=getAllPets();
			
			map=new char[16][16];
			for(int i=0;i<16;i++)
				for(int j=0;j<16;j++)
					map[i][j]=0;
			
			int[] used=new int[X*Y];
		
			for(int i=0;i<parr.length;i++)
			{
				int newpos=(int)(Math.random()*X*Y);
				for(int j=0,k=0;;j++)
				{
					if(used[j]==0)
						k++;
					if(k-1==newpos)
					{
						newpos=j;
						used[j]=1;
						break;
					}
				}
				int x=newpos/X;
				int y=newpos%X;
				
				map[x][y]=(char)(65+i);
				
				
				POOCoordinate now=new CoordinateSample(x,y);
				allcoordinate.add(now);
			}
			
			show();
		}
		while(((BetweenPet)parr[turn]).getHPBetween()==0)
		{//System.out.println(""+turn+" is dead");
			turn++;
			turn%=parr.length;
		}
		
		System.out.println("It's "+ ((BetweenPet)parr[turn]).getNameBetween() +"'s turn:");
		
		int xo=((CoordinateSample)allcoordinate.get(turn)).getX(),yo=((CoordinateSample)allcoordinate.get(turn)).getY();
		
		allcoordinate.set(  turn , ((BetweenPet)parr[turn]).moveBetween(this)  );
		
		int xp=((CoordinateSample)allcoordinate.get(turn)).getX(),yp=((CoordinateSample)allcoordinate.get(turn)).getY();
		
		char tmp;
		tmp=map[xo][yo];
		map[xo][yo]=map[xp][yp];
		map[xp][yp]=tmp;
		
		
		POOSkill sk= ((BetweenPet)parr[turn]).actBetween(this);
		POOPet sk2= ((BetweenPet)parr[turn]).act2Between();
		sk.act(sk2);
		
		turn++;
		turn%=parr.length;
		
		int cnt=0;
		for(int i=0;i<parr.length;i++)
			if(  ((BetweenPet)parr[i]).getHPBetween() > 0  )
				cnt++;
		if(cnt>1)return true;
		else 
		{
			show();
			for(int i=0;i<parr.length;i++)
				if(  ((BetweenPet)parr[i]).getHPBetween() > 0  )
				{
					System.out.println( "\nThe winner is " + ((BetweenPet)parr[i]).getNameBetween() + "!! Game Over.\n");
					break;
				}
			
			return false;
		}
	}
	public void show()
	{
		System.out.print("\033[2J");
		System.out.print("\033[0;0H");
		
		System.out.printf("Round %d:\n\n",round);
		round++;
		
		System.out.print("   ");
		for(int i=0;i<16;i++)
			System.out.printf("|%3d",i);
		System.out.println("");
		
		for(int i=0;i<16;i++)
		{
			for(int j=0;j<16;j++)System.out.print("---+");
			System.out.println("---");
			
			System.out.printf("%3d",i);
			for(int j=0;j<16;j++)
				if(map[i][j]==0)System.out.print("|   ");
				else System.out.printf("| %c ",map[i][j]);
			System.out.println("");
		}
		System.out.println("");
		
		for(int i=0;i<parr.length;i++)
		{
			System.out.printf("%c.",(char)(65+i));
			System.out.print(((BetweenPet)parr[i]).getNameBetween() + " ");
			System.out.printf("(%2d",((CoordinateSample)allcoordinate.get(i)).getX());
			System.out.printf(",%2d) ",((CoordinateSample)allcoordinate.get(i)).getY());
			System.out.printf("HP:%d ",((BetweenPet)parr[i]).getHPBetween());
			System.out.printf("MP:%d ",((BetweenPet)parr[i]).getMPBetween());
			System.out.printf("AGI:%d\n",((BetweenPet)parr[i]).getAGIBetween());
		}
		
		System.out.println("");
		
	}
	public POOCoordinate getPosition(POOPet p)
	{
		for(int i=0;i<parr.length;i++)
			if(parr[i].equals(p))
				return allcoordinate.get(i);
		return null;
	}
}