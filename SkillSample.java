import ntu.csie.oop13spring.POOFight;
import ntu.csie.oop13spring.POOArena;
import ntu.csie.oop13spring.POOCoordinate;
import ntu.csie.oop13spring.POOPet;
import ntu.csie.oop13spring.POOAction;
import ntu.csie.oop13spring.POOSkill;
class Attack extends POOSkill
{
	public void act(POOPet pet)
	{
		int h=((BetweenPet)pet).getHPBetween();
		if(h>0)
		{
			((BetweenPet)pet).setHPBetween(h-1);
		}
	}
}
class BigBite extends POOSkill
{
	public void act(POOPet pet)
	{
		int h=((BetweenPet)pet).getHPBetween();
		if(h<3)
		{
			((BetweenPet)pet).setHPBetween(0);
		}
		else
			((BetweenPet)pet).setHPBetween(h-3);
	}
}

class Shoot extends POOSkill
{
	public void act(POOPet pet)
	{
		int h=((BetweenPet)pet).getHPBetween();
		if(h<5)
		{
			((BetweenPet)pet).setHPBetween(0);
		}
		else
			((BetweenPet)pet).setHPBetween(h-5);
	}
}


class Nothing extends POOSkill
{
	public void act(POOPet pet)
	{
	}
}