import ntu.csie.oop13spring.POOFight;
import ntu.csie.oop13spring.POOArena;
import ntu.csie.oop13spring.POOCoordinate;
import ntu.csie.oop13spring.POOPet;
import ntu.csie.oop13spring.POOAction;
import ntu.csie.oop13spring.POOSkill;
public class CoordinateSample extends POOCoordinate
{
	public CoordinateSample(int nx,int ny)
	{
		x=nx;
		y=ny;
	}
	public boolean equals(POOCoordinate other)
	{
		if(x==((CoordinateSample)other).getX()&&y==((CoordinateSample)other).getY())return true;
		return false;
	}
	public int getX(){return x;}
	public int getY(){return y;}
}