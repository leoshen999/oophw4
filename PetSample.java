import java.util.Scanner;
import ntu.csie.oop13spring.POOFight;
import ntu.csie.oop13spring.POOArena;
import ntu.csie.oop13spring.POOCoordinate;
import ntu.csie.oop13spring.POOPet;
import ntu.csie.oop13spring.POOAction;
import ntu.csie.oop13spring.POOSkill;
abstract class BetweenPet extends POOPet
{
	public POOCoordinate moveBetween(POOArena arena){return move(arena);}
	public abstract POOSkill actBetween(POOArena arena);
	public abstract POOPet act2Between();
	public String getNameBetween(){return getName();}
	public int getHPBetween(){return getHP();}
	public int getMPBetween(){return getMP();}
	public int getAGIBetween(){return getAGI();}
	
	public boolean setNameBetween(String a){return setName(a);}
	public boolean setHPBetween(int a){return setHP(a);}
	public boolean setMPBetween(int a){return setMP(a);}
	public boolean setAGIBetween(int a){return setAGI(a);}
}





public class PetSample extends BetweenPet
{
	private Scanner scanner;
	public String command;
	public PetSample()
	{
		
		setHP(100);
		setMP(40);
		setAGI(6);
		
		System.out.print("Insert your PetSample's name: ");
		
		scanner = new Scanner(System.in);
		while(!setName(scanner.nextLine()))
			System.out.print("Retry again: ");
		scanner=null;
	}
	
	protected POOAction act(POOArena arena){return null;}
	
	public POOPet target;
	public POOPet act2Between()
	{
		return target;
	}
	
	public POOSkill actBetween(POOArena arena)
	{
		POOPet[] parr=arena.getAllPets();
		
		
		
		
		
		int x=0,y=0;
		for(int i=0;i<parr.length;i++)
			if(((BetweenPet)parr[i]).equals(this))
			{
				x=((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getX();
				y=((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getY();
				break;
			}
		
		System.out.print("Action!!\n");
		System.out.print("   1. Attack (Distance:1 Damage:1 Cost:0)\n");
		System.out.print("   2. BigBite (Distance:1 Damage:3 Cost:5)\n");
		System.out.print("   3. Nothing\n");
		System.out.print("What to do: ");
		scanner = new Scanner(System.in);
		while(true)
		{
			command=scanner.nextLine();
			int c=Integer.parseInt(command);
			
			if(c==1)
			{
				System.out.print("to whom: ");
				command=scanner.nextLine();
				for(int i=0;i<parr.length;i++)
				{
					if(((BetweenPet)parr[i]).getNameBetween().equals(command))
					{
						int nx=((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getX();
						int ny=((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getY();
						
						if(Math.abs(nx-x)+Math.abs(ny-y)<=1)
						{
							POOSkill ac=new Attack();
							target=parr[i];
							scanner=null;
							
							System.out.println( getName() +" attacks " + ((BetweenPet)parr[i]).getNameBetween() +"!!");
							
							
							return ac;
						}
					}
				}
			}
			else if(c==2)
			{
				int m=getMP();
				if(m>4)
				{
					System.out.print("to whom: ");
					command=scanner.nextLine();
					for(int i=0;i<parr.length;i++)
					{
						if(((BetweenPet)parr[i]).getNameBetween().equals(command))
						{
							int nx=((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getX();
							int ny=((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getY();
							
							if(Math.abs(nx-x)+Math.abs(ny-y)<=1)
							{
								setMP(m-5);
								
								POOSkill ac=new BigBite();
								target=parr[i];
								scanner=null;
								
								System.out.println( getName() +" bites " + ((BetweenPet)parr[i]).getNameBetween() +"!!");
								
								
								return ac;
							}
						}
					}
				}
			}
			else if(c==3)
			{
				POOSkill ac=new Nothing();
				target=null;
				scanner=null;
				
				System.out.print(getName() + " does nothing\n");
				
				return ac;
			}
			System.out.print("Retry again: ");
		}
	}
	
	protected POOCoordinate move(POOArena arena)
	{
		POOPet[] parr=arena.getAllPets();
		
		
		System.out.print("Move!!\n");
		System.out.print("Where to go: ");
		int x=0,y=0;
		for(int i=0;i<parr.length;i++)
			if(((BetweenPet)parr[i]).equals(this))
			{
				x=((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getX();
				y=((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getY();
				break;
			}
		scanner = new Scanner(System.in);
		
		while(true)
		{
			command=scanner.nextLine();
			int index=command.indexOf(" ");
			int nx=Integer.parseInt(command.substring(0,index));
			int ny=Integer.parseInt(command.substring(index+1));
			
			for(int i=0;i<((ArenaSample)arena).allcoordinate.size();i++)
				if( (!((BetweenPet)parr[i]).equals(this))&&((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getX()==nx&&((CoordinateSample)((ArenaSample)arena).allcoordinate.get(i)).getY()==ny )
					break;
				else if(i==((ArenaSample)arena).allcoordinate.size()-1&&nx>=0&&nx<((ArenaSample)arena).X&&ny>=0&&ny<((ArenaSample)arena).Y)
					if(Math.abs(nx-x)+Math.abs(ny-y)<=getAGI())
					{
						scanner=null;
						System.out.print(getName());
						System.out.printf(" moves to (%2d,%2d).\n",nx,ny);
						return new CoordinateSample(nx,ny);
					}
			
			System.out.print("Retry again: ");
		}
	}
}